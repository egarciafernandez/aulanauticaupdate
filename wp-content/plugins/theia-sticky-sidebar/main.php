<?php
/*
Plugin Name: Theia Sticky Sidebar
Description: Make your sidebar sticky and permanently visible.
Author: Liviu Cristian Mirea Ghiban
Version: 1.1.0
*/

/*
 * Copyright 2013, Theia Sticky Sidebar, Liviu Cristian Mirea Ghiban.
 */

/*
 * Plugin version. Used to forcefully invalidate CSS and JavaScript caches by appending the version number to the
 * filename (e.g. "style.css?ver=TSS_VERSION").
 */
define('TSS_VERSION', '1.1.0');

// Include other files.
include(dirname(__FILE__) . '/TssMisc.php');
include(dirname(__FILE__) . '/TssOptions.php');
include(dirname(__FILE__) . '/TssAdmin.php');
include(dirname(__FILE__) . '/TssTemplates.php');

// Add hooks.
add_action('wp_footer', 'TssMisc::wp_footer');
add_action('wp_enqueue_scripts', 'TssMisc::wp_enqueue_scripts');
add_action('admin_init', 'TssAdmin::admin_init');
add_action('admin_menu', 'TssAdmin::admin_menu');

// Initialize plugin options.
TssOptions::initOptions();