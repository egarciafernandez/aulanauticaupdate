<?php
/*
 * Copyright 2013, Theia Sticky Sidebar, Liviu Cristian Mirea Ghiban.
 */

class TssAdmin {
	public static function admin_init() {
		register_setting('tss_options_general', 'tss_general', 'TssAdmin::validate');
	}

	public static function admin_menu() {
		add_options_page('Theia Sticky Sidebar Settings', 'Theia Sticky Sidebar ', 'manage_options', 'tss', 'TssAdmin::do_page');
	}

	public static function do_page() {
		$tabs = array(
			'general' => array(
				'title' => __("General", 'theia-sticky-sidebar'),
				'class' => 'General'
			),
			'about' => array(
				'title' => __("About", 'theia-sticky-sidebar'),
				'class' => 'About'
			)
		);
		if (array_key_exists('tab', $_GET) && array_key_exists($_GET['tab'], $tabs)) {
			$currentTab = $_GET['tab'];
		}
		else {
			$currentTab = 'general';
		}
		?>

		<div class="wrap" xmlns="http://www.w3.org/1999/html">
			<div id="icon-options-general" class="icon32"><br></div>
			<h2>Theia Sticky Sidebar</h2>
			<h2 class="nav-tab-wrapper">
				<?php
				foreach ($tabs as $id => $tab) {
				    $class = 'nav-tab';
				    if ($id == $currentTab) {
				        $class .= ' nav-tab-active';
				    }
				    ?>
				    <a href="?page=tss&tab=<?php echo $id; ?>" class="<?php echo $class; ?>"><?php echo $tab['title']; ?></a>
				    <?php
				}
				?>
			</h2>
			<?php
			$class = 'TssAdmin_' . $tabs[$currentTab]['class'];
			require $class  . '.php';
			$page = new $class;
			$page->echoPage();
			?>
		</div>
		<?php
	}

	public static function validate($input) {
		return $input;
	}
}