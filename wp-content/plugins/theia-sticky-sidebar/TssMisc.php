<?php
/*
 * Copyright 2013, Theia Sticky Sidebar, Liviu Cristian Mirea Ghiban.
 */

class TssMisc {
	const
		META_POSITION_PICKER = 'theiaStickySidebar_positionPicker',
		META_POSITION = 'theiaStickySidebar_position';

	public static function wp_enqueue_scripts() {
		if (self::isDisabled()) {
			return;
		}

		// Sidebar JS
		wp_register_script('theiaStickySidebar-sidebar.js', plugins_url('js/sidebar.js', __FILE__), array('jquery'), TSS_VERSION, true);
		wp_enqueue_script('theiaStickySidebar-sidebar.js');
	}

	public static function wp_footer() {
		if (self::isDisabled()) {
			return;
		}

		$options = array(
			'sidebarSelector' => TssOptions::get('sidebarSelector'),
			'containerSelector' => TssOptions::get('containerSelector'),
			'additionalMarginTop' => TssOptions::get('additionalMarginTop'),
			'additionalMarginBottom' => TssOptions::get('additionalMarginBottom'),
			'updateSidebarHeight' => TssOptions::get('updateSidebarHeight'),
			'minWidth' => TssOptions::get('minWidth')
		);
		
		$options['containerSelector'] = explode(',', $options['containerSelector']);
		
		?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				var theiaStickySidebarOptions = <?php echo json_encode($options) ?>;
				initTheiaStickySidebar(theiaStickySidebarOptions);
			});
		</script>
		<?php
	}

	public static function isDisabled() {
		return
			(TssOptions::get('disableOnHomePage') && is_home()) ||
			(TssOptions::get('disableOnCategoryPages') && is_category())  ||
			(TssOptions::get('disableOnPages') && is_page())  ||
			(TssOptions::get('disableOnPosts') && is_single());
	}
}
