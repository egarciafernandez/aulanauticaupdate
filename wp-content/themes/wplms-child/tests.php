<?php

/**
 * Template Name: Tests
 * FILE: tests.php 
 * Created on Apr 2, 2013 at 3:07:11 PM 
 * Author: mark
 */


get_header();


if ( have_posts() ) : while ( have_posts() ) : the_post();

$title=get_post_meta(get_the_ID(),'vibe_title',true);
if(vibe_validate($title)){
?>
<section id="title">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="pagetitle">
                    <h1><?php the_title(); ?></h1>
                    <?php the_sub_title(); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-4">
                <?php
                    $breadcrumbs=get_post_meta(get_the_ID(),'vibe_breadcrumbs',true);
                    if(vibe_validate($breadcrumbs))
                        vibe_breadcrumbs(); 
                ?>
            </div>
        </div>
    </div>
</section>
<?php
}

    $v_add_content = get_post_meta( $post->ID, '_add_content', true );
 
?>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tests-div">
                    <?php
                        the_content();

                        $course_ID_tabla = array(
                            0 => array(1204),
                            1 => array(1913),
                            2 => array(2249,2251),
                            3 => array(1977)
                        );

                        echo '<div class="row">';
                        foreach ($course_ID_tabla as $columna){
                            echo "<div class='col-md-3'>";
                            foreach ($columna as $course){
                                echo '<h4 style="height: 50px;"><a style="color:#003366" href="'.get_the_permalink($course).'">'.get_the_title($course).'</a></h4>';
                                $course_curriculum = bp_course_get_full_course_curriculum($course); 
                                if(!empty($course_curriculum)){
                                    foreach($course_curriculum as $lesson){ 
                                        if($lesson['type']=='quiz'){
                                            ?>
                                            <div class="course_lesson">
                                                <div class="title_quiz"><?php echo apply_filters('wplms_curriculum_course_quiz','<a href="'.get_permalink($lesson['id']).'?id='.$course.'">'. $lesson['title'].'</a>',$lesson['id'],$course); ?></div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                echo '<br>';
                            }
                            echo "</div>";
                        }
                        echo '</div>';

                        /*
                        $course_IDs = array (1913,1977,1204,2249,2251);
                        foreach ($course_IDs as $course){
                            echo '<h4><a style="color:#003366" href="'.get_the_permalink($course).'">'.get_the_title($course).'</a></h4>';
                            $course_curriculum = bp_course_get_full_course_curriculum($course); 
                            if(!empty($course_curriculum)){
                                foreach($course_curriculum as $lesson){ 
                                    if($lesson['type']=='quiz'){
                                        ?>
                                        <div class="course_lesson">
                                            <div class="curriculum-icon"><i class="icon-<?php echo $lesson['icon']; ?>"></i></div>
                                            <div class="title_quiz"><?php echo apply_filters('wplms_curriculum_course_quiz','<a href="'.get_permalink($lesson['id']).'?id='.$course.'">'. $lesson['title'].'</a>',$lesson['id'],$course); ?></div>
                                            <div class="time_quiz"><?php echo $lesson['duration']; ?></div>
                                        </div>
                                        <br />
                                        <?php
                                        do_action('wplms_curriculum_course_quiz_details',$lesson);
                                    }
                                }
                            }
                        }
                        */
                     ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
endwhile;
endif;
?>
</div>
<?php
get_footer();
?>
